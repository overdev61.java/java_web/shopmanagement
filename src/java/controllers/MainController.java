package controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "MainController", urlPatterns = {"/MainController"})
public class MainController extends HttpServlet {

    private static final String ERROR = "error.jsp";
    private static final String HOME_PAGE = "viewproductservlet";
    private static final String LOGIN = "Login.jsp";
    private static final String CHECK_LOGIN = "login";
    private static final String REGISTER = "RegisterSerlvet";
    private static final String VIEW_PRODUCT = "viewproductservlet";
    private static final String UPDATE = "UpdateServlet";
    private static final String CREATE = "CreateServlet";
    private static final String SEARCHBYCATE = "SearchProductByCategoryServlet";
    private static final String ADD_TO_CART = "AddToCartController";
    private static final String DELETE_CART = "DeleteCartServlet";
    private static final String BUY = "BuyController";
    private static final String SEARCH_BY_NAME = "SearchByNameServlet";
    private static final String LOGOUT = "logout";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        try {
            String action = request.getParameter("action");
            if (action == null) {
                url = HOME_PAGE;
            }
            if (action.equals("Login")) {
                url = LOGIN;
            }
            if (action.equals("CheckLogin")) {
                url = CHECK_LOGIN;
            }
            if (action.equals("viewProduct")) {
                url = VIEW_PRODUCT;
            }
            if (action.equals("Register")) {
                url = REGISTER;
            }
            if (action.equals("Update")) {
                url = UPDATE;
            }
            if (action.equals("Create")) {
                url = CREATE;
            }
            if (action.equals("SearchByCate")) {
                url = SEARCHBYCATE;
            }
            if (action.equals("AddToCart")) {
                url = ADD_TO_CART;
            }
            if (action.equals("DeleteCart")) {
                url = DELETE_CART;
            }
            if (action.equals("Buy")) {
                url = BUY;
            }
            if (action.equals("SearchByName")) {
                url = SEARCH_BY_NAME;
            }
            if (action.equals("Logout")) {
                url = LOGOUT;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
