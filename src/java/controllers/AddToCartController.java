package controllers;

import daos.ProductDAO;
import dtos.CartDTO;
import dtos.Product;
import dtos.User;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "AddToCartController", urlPatterns = {"/AddToCartController"})
public class AddToCartController extends HttpServlet {

    private static final String ERROR = "viewproductservlet";
    private static final String LOGIN = "Login.jsp";
    private static final String SUCCESS = "viewproductservlet";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        HttpSession session = request.getSession();
        try {
            User user = (User) session.getAttribute("USER_DATA");
            if (user == null) {
                request.setAttribute("ERROR", "You need to login first!");
                url = LOGIN;
            } else if (user.isIsAdmin() == true) {
                request.setAttribute("ERROR", "Your action was denied!!");
            } else {
                ProductDAO pDAO = new ProductDAO();
                String id = request.getParameter("id");
                String name = pDAO.getProductById(id).getName();
                int price = pDAO.getProductById(id).getPrice();
                int quantity = 1;
                Product pro = new Product(id, name, price, quantity);
                CartDTO cart = (CartDTO) session.getAttribute("CART");
                if (cart == null) {
                    cart = new CartDTO();
                }
                boolean check = cart.add(pro);
                if (check == true) {
                    session.setAttribute("CART", cart);
                    url = SUCCESS;
                    request.setAttribute("MSG", "Add " + name + " success!!");
                }
            }
        } catch (Exception e) {
            log("Error at AddToCartController" + e.toString());
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
