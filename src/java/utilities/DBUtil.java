
package utilities;


import java.sql.Connection;
import java.sql.DriverManager;


public class DBUtil {
    
     public static Connection getConnection(){
        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url= "jdbc:sqlserver://localhost:1433;databaseName=Assignment";
            Connection con= DriverManager.getConnection(url, "sa", "12345");
            return con;
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
 