/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import daos.UserDao;
import dtos.ErrorUser;
import dtos.User;
import java.io.IOException;
import java.util.concurrent.ThreadLocalRandom;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author hoaia
 */
@WebServlet(name = "RegisterSerlvet", urlPatterns = {"/RegisterSerlvet"})
public class RegisterSerlvet extends HttpServlet {

    private static final String ERROR = "register.jsp";
    private static final String SUCCESS = "Login.jsp";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        try {
            User user;
            ErrorUser errUser = new ErrorUser();
            UserDao uDAO = new UserDao();
            int getRandomNumber = ThreadLocalRandom.current().nextInt(0, 10000); //get random number
            do {
                getRandomNumber = getRandomNumber + 1;
            } while (uDAO.checkDuplicateId(String.valueOf(getRandomNumber)) == true); //loop check duplicate ID

            String id = String.valueOf(getRandomNumber);
            String name = request.getParameter("name");
            String password = request.getParameter("password");
            String confirmPassword = request.getParameter("confirmPassword");
            String email = request.getParameter("email");
            String phoneNumber = request.getParameter("phoneNumber");
            boolean isAdmin = false;
            String address = request.getParameter("address");
            boolean check = true;
            if (name.trim().length() <= 0) {
                check = false;
                errUser.setErrName("Name can not be empty!!");
            }
            if (password.trim().length() <= 0 || password.trim().length() > 10) {
                check = false;
                errUser.setErrPassword("Password must be higher than 0 and less than 10 character!!");
            }
            if (!password.equals(confirmPassword)) {
                check = false;
                errUser.setErrConfirmPassword("Confirm Password is not correct");
            }
            if (email.trim().length() <= 0) {
                check = false;
                errUser.setErrEmail("Email can not be empty!!");
            }
            if (phoneNumber.trim().length() <= 0 || phoneNumber.trim().length() > 11) {
                check = false;
                errUser.setErrPhoneNumber("Phone Number can not be empty");
            }
            if (address.trim().length() <= 0) {
                check = false;
                errUser.setErrAddress("Address can not be empty");
            }
            if (check == true) {
                boolean checkDuplicateId = uDAO.checkDuplicateId(id); //check duplicate ID
                boolean checkDuplicateEmail = uDAO.checkDuplicateEmail(email); //check duplicate Email
                if (checkDuplicateEmail == true) {
                    errUser.setErrEmail("Email is already Existed!!");
                    request.setAttribute("ERROR", errUser);
                    url = ERROR;
                } else {
                    user = new User(id, name, password, email, phoneNumber, isAdmin, address);
                    boolean checkResgister = uDAO.createUser(user); //check xem create thanh cong hay ko
                    if (checkResgister) {
                        url = SUCCESS;
                        request.setAttribute("MSG", "Register successfully!!");
                    }
                }
            } else {
                request.setAttribute("ERROR", errUser);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
