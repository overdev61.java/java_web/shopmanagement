<%-- 
    Document   : register
    Created on : Jun 30, 2022, 7:08:27 PM
    Author     : hoaia
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Register Page</title>
    </head>
    <body>
        <h1>Register</h1>
        <form action="MainController" method="POST" class="form-signup">
            <h1 class="h3 mb-3 font-weight-normal" style="text-align: center"> Sign up</h1>
            <h2 class="h3 mb-3 font-weight-normal" style="text-align: center"> ${MSG}</h2>
            <input name="email" type="text" id="user-name" class="form-control" placeholder="Email" required="" autofocus="">
            <input name="password" type="password" id="user-pass" class="form-control" placeholder="Password" required autofocus="">
            <input name="confirmPassword" type="password" id="user-repeatpass" class="form-control" placeholder="Confirm Password" required autofocus="">
            <input name="name" type="text" id="user-repeatpass" class="form-control" placeholder="Name" required autofocus="">
            <input name="phoneNumber" type="number" id="user-repeatpass" class="form-control" placeholder="Phone Number" required autofocus="">
            <input name="address" type="text" id="user-repeatpass" class="form-control" placeholder="Address" required autofocus="">
            <button class="btn btn-primary btn-block" type="submit" name="action" value="Register"><i class="fas fa-user-plus"></i> Sign Up</button>
            <a href="#" id="cancel_signup"><i class="fas fa-angle-left"></i> Back</a>
        </form>
        <a href="MainController&action=Login">Login</a>
    </body>
</html>
