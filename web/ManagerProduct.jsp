<%-- 
    Document   : ManagerProduct
    Created on : Dec 28, 2020, 5:19:02 PM
    Author     : trinh
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Admin Page</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="css/manager.css" rel="stylesheet" type="text/css"/>
        <style>
            img{
                width: 200px;
                height: 120px;
            }
        </style>
    </head>    

    <body>
        <font color="red">${ERROR}</font>
        <font color="green">${MSG}</font>
        <c:if test="${sessionScope.USER_DATA.isAdmin != true || sessionScope.USER_DATA == null}">
            <c:redirect url="MainController" />
        </c:if>
        <div class="container">
            <div class="table-wrapper">
                <div class="table-title">
                    <div class="row">
                        <div class="col-sm-6">
                            <h2>Manage <b>Product</b></h2>
                        </div>

                        <div class="col-sm-6">
                            <a class="btn btn-success" href="logout" >
                                <span>Logout</span>
                            </a>
                            <a href="#addEmployeeModal"  class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Add New Product</span></a>
                            <a href="MainController?action=viewProduct" class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE15C;</i> <span>Home Page</span></a>						
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Image</th>
                            <th>Description</th>
                            <th>Create Date</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Status</th>  
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        <c:forEach items="${listProduct}" var="l">
                            <tr>
                                <td>${l.id}</td>
                                <td>${l.name}</td>
                                <td><img src="${l.imageLink}" width="30px" /></td>
                                <td>${l.description}</td>
                                <td>${l.createDate}</td>
                                <td>${l.price}</td>
                                <td>${l.quantity}</td>
                                <c:if test="${l.status == 'false'}">
                                    <td>Unavailable</td>
                                </c:if>
                                <c:if test="${l.status == 'true'}">
                                    <td>Available</td>
                                </c:if> 
                                <td> <a href="#${l.id}"  class="btn btn-success" data-toggle="modal"><span>Update</span></a></td>
                            </tr>   
                            <!-- Delete Modal HTML -->
                        <div id="${l.id}" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form action="MainController" method="POST">
                                        <div class="modal-header">						
                                            <h4 class="modal-title">Edit Product</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        </div>
                                        <div class="modal-body">	
                                            <div class="form-group">
                                                ID: 
                                                <input type="text" name="id" value="${l.id}" readonly="" disable/>
                                            </div>
                                            <div class="form-group">
                                                Name:
                                                <input type="text" name="name" value="${l.name}" class="form-control" required/>
                                            </div>
                                            <div class="form-group">
                                                Image Link: 
                                                <input type="text" name="imageLink" value="${l.imageLink}" class="form-control" required/>
                                            </div>
                                            <div class="form-group">
                                                <label>Description</label>
                                                <textarea type="text" name="description" value="${l.description}" class="form-control" required>${l.description}</textarea>
                                            </div>
                                            <div class="form-group">
                                                Price: 
                                                <input type="number" name="price" value="${l.price}" class="form-control" required/>
                                            </div>	
                                            <div class="form-group">
                                                Quantity: 
                                                <input type="number" name="quantity" value="${l.quantity}" class="form-control" required/>
                                            </div>
                                            <div class="form-group">
                                                Status: 
                                                <select name="status">
                                                    <option value="false">Unavailable</option>
                                                    <option value="true">Available</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                                            <input type="submit" class="btn btn-info" name="action" value="Update"/>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                    <c:set scope="page" var="productUpdate" value="${l}"/>

                    </tbody>
                </table>
            </div>
        </div>

    </form>
    <div id="addEmployeeModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="MainController" method="POST">
                    <div class="modal-header">						
                        <h4 class="modal-title">Add Product</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">					
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" name="name" value="" class="form-control" required />
                        </div>
                        <div class="form-group">
                            <label>Image</label>
                            <input type="text" name="imageLink" value="" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea type="text" name="description" value="" class="form-control" required></textarea>
                        </div>
                        <div class="form-group">
                            <label>Price</label>
                            <input name="price" type="number" value="" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Quantity</label>
                            <input type="number" name="quantity" value="" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label>Status</label>
                            <select name="status">
                                <option value="false" >Unavailable</option>
                                <option value="true">Available</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Category</label>
                            <select name="categoryId">
                                <option value="1">Smart Phone</option>
                                <option value="2">Laptop</option>
                                <option value="3">Tablet</option>
                                <option value="4">Smart Watch</option>
                                <option value="5">Ear Phone</option>
                            </select>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <input type="submit" name="action" class="btn btn-success" value="Create"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
   
    <script src="js/manager.js" type="text/javascript"></script>
    <script>

    </script>
</body>
</html>