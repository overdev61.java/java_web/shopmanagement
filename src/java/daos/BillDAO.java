package daos;

import dtos.BillDTO;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import utilities.DBUtil;

public class BillDAO {

    private Connection con = null;
    private PreparedStatement pstm = null;
    private ResultSet rs = null;

    public void closeConnection() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (pstm != null) {
            pstm.close();
        }
        if (con != null) {
            con.close();
        }
    }

    public boolean add(String id, String userID, int total, boolean paymentMethod, Date createDate) throws SQLException {
        boolean check = false;
        try {
            con = DBUtil.getConnection();
            if (con != null) {
                String sql = " INSERT INTO tblBill(id, userID, total, paymentMethod, createDate) "
                        + " VALUES(?,?,?,?,?) ";
                pstm = con.prepareStatement(sql);
                pstm.setString(1, id);
                pstm.setString(2, userID);
                pstm.setInt(3, total);
                pstm.setBoolean(4, paymentMethod);
                pstm.setDate(5, createDate);
                check = pstm.executeUpdate() > 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return check;
    }

    public int getLastBillID() throws Exception {
        int id = 0;
        try {
            con = DBUtil.getConnection();
            String sql = "SELECT MAX(CAST(id AS INT)) AS id FROM dbo.tblBill";

            pstm = con.prepareStatement(sql);
            rs = pstm.executeQuery();
            if (rs.next()) {
                id = rs.getInt("id");
            }
        } finally {
            closeConnection();
        }
        return id;
    }
}
