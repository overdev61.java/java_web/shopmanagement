package dtos;

import java.sql.Date;



public class BillDTO {

    private String id;
    private String userId;
    private int total;
    private boolean paymentMethod;
    private Date createDate;

    public BillDTO() {
    }

    public BillDTO(String id, String userId, int total, boolean paymentMethod, Date createDate) {
        this.id = id;
        this.userId = userId;
        this.total = total;
        this.paymentMethod = paymentMethod;
        this.createDate = createDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public boolean isPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(boolean paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    }
