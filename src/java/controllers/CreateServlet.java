package controllers;

import daos.CategoryDAO;
import daos.ProductDAO;
import dtos.ErrorProduct;
import dtos.Product;
import java.io.IOException;
import java.time.LocalDate;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "CreateServlet", urlPatterns = {"/CreateServlet"})
public class CreateServlet extends HttpServlet {

    private static final String ERROR = "create_view.jsp";
    private static final String SUCCESS = "viewproductservlet";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        try {
            ErrorProduct errProduct = new ErrorProduct();
            ProductDAO pDAO = new ProductDAO();
            CategoryDAO cDAO = new CategoryDAO();
            String id = String.valueOf(pDAO.getLastProductID() + 1);
            String name = request.getParameter("name");
            String imageLink = request.getParameter("imageLink");
            int price = Integer.parseInt(request.getParameter("price"));
            int quantity = Integer.parseInt(request.getParameter("quantity"));
            String description = request.getParameter("description");
            String createDate = LocalDate.now().toString();
            boolean status = Boolean.parseBoolean(request.getParameter("status"));
            String categoryId = request.getParameter("categoryId");

            boolean check = true;
            if (name.isEmpty()) {
                check = false;
                errProduct.setErrName("Product's name can not be empty");
            }
            if (imageLink.isEmpty()) {
                check = false;
                errProduct.setErrImageLink("Image Link can not be empty");
            }
            if (price < 1) {
                check = false;
                errProduct.setErrPrice("Price  must be higher than 0");
            }
            if (quantity < 1) {
                check = false;
                errProduct.setErrQuantity("Quantity must higher than 0");
            }
            if (check) {
                Product p = new Product(id, name, imageLink, description, createDate, price, quantity, status, cDAO.getCategoryID(categoryId));
                if (pDAO.createProduct(p) == true) {
                    url = SUCCESS;
                } else {
                    request.setAttribute("ERROR", "Insert failed!!");
                }
            } else {
                request.setAttribute("ERROR", errProduct);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
