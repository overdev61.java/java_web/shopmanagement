package daos;

import dtos.BillDetailDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import utilities.DBUtil;

public class BillDetailDAO {

    private Connection con = null;
    private PreparedStatement pstm = null;
    private ResultSet rs = null;

    public void closeConnection() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (pstm != null) {
            pstm.close();
        }
        if (con != null) {
            con.close();
        }
    }

    public boolean add(BillDetailDTO billDetailDTO) throws SQLException {
        boolean check = false;
        try {
            con = DBUtil.getConnection();
            if (con != null) {
                String sql = " INSERT INTO tblBillDetail(id, billID, productID, quantity, price) "
                        + " VALUES(?,?,?,?,?) ";
                pstm = con.prepareStatement(sql);
                pstm.setString(1, billDetailDTO.getId());
                pstm.setString(2, billDetailDTO.getBillID());
                pstm.setString(3, billDetailDTO.getProduct().getId());
                pstm.setInt(4, billDetailDTO.getQuantity());
                pstm.setInt(5, billDetailDTO.getPrice());
                check = pstm.executeUpdate() > 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return check;
    }

    public int getLastBillDetailID() throws Exception {
        int id = 0;
        try {
            con = DBUtil.getConnection();
            String sql = "SELECT MAX(CAST(id AS INT)) AS id FROM dbo.tblBillDetail";

            pstm = con.prepareStatement(sql);
            rs = pstm.executeQuery();
            if (rs.next()) {
                id = rs.getInt("id");
            }
        } finally {
            closeConnection();
        }
        return id;
    }
}
