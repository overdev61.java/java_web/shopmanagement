package controllers;

import daos.BillDAO;
import daos.BillDetailDAO;
import dtos.BillDTO;
import dtos.BillDetailDTO;
import dtos.CartDTO;
import dtos.Product;
import dtos.User;
import java.io.IOException;
import java.sql.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "BuyController", urlPatterns = {"/BuyController"})
public class BuyController extends HttpServlet {

    private static final String ERROR = "viewproductservlet";
    private static final String LOGIN = "Login.jsp";
    private static final String SUCCESS = "viewproductservlet";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        String url = ERROR;
        try {
            User user = (User) session.getAttribute("USER_DATA");
            CartDTO  cart = (CartDTO) session.getAttribute("CART");
            if (user == null) {
                request.setAttribute("ERROR", "You need to login first");
                url = LOGIN;
            } else {
                BillDAO bDAO = new BillDAO();
                String billID = String.valueOf(bDAO.getLastBillID() + 1);
                int total = 0;
                for (Product pro : cart.getCart().values()) {
                    total += (pro.getQuantity()*pro.getPrice());
                }
                boolean paymentMethod = Boolean.parseBoolean(request.getParameter("paymentMethod"));
                Date createDate = new Date(System.currentTimeMillis());
                BillDTO bill = new BillDTO(billID, user.getId(), total, paymentMethod, createDate);
                System.out.println(bill);

                if (bDAO.add(billID, user.getId(), total, paymentMethod, createDate) == true) {
                    BillDetailDTO billDetailDTO;
                    BillDetailDAO bdDAO = new BillDetailDAO();
                    String billDetailID = String.valueOf(bdDAO.getLastBillDetailID() + 1);
                    for (Product pro : cart.getCart().values()) {
                        billDetailDTO = new BillDetailDTO(billDetailID, billID, pro, pro.getQuantity(), pro.getPrice());
                        bdDAO.add(billDetailDTO);
                    }
                    session.removeAttribute("CART");
                    request.setAttribute("MSG", "Buy successfully");
                    url = SUCCESS;
                } else {
                    request.setAttribute("ERROR", "Buy unsuccessfully");
                }
            }
        } catch (Exception e) {
            log("Error at BuyController " + e.toString());
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
