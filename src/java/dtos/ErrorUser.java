/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dtos;

/**
 *
 * @author hoaia
 */
public class ErrorUser {
    private String errEmail;
    private String errPassword;
    private String errConfirmPassword;
    private String errName;
    private String errPhoneNumber;
    private String errAddress;

    public ErrorUser() {
    }

    public ErrorUser(String errEmail, String errPassword, String errConfirmPassword, String errName, String errPhoneNumber, String errAddress) {
        this.errEmail = errEmail;
        this.errPassword = errPassword;
        this.errConfirmPassword = errConfirmPassword;
        this.errName = errName;
        this.errPhoneNumber = errPhoneNumber;
        this.errAddress = errAddress;
    }

    public String getErrEmail() {
        return errEmail;
    }

    public void setErrEmail(String errEmail) {
        this.errEmail = errEmail;
    }

    public String getErrPassword() {
        return errPassword;
    }

    public void setErrPassword(String errPassword) {
        this.errPassword = errPassword;
    }

    public String getErrConfirmPassword() {
        return errConfirmPassword;
    }

    public void setErrConfirmPassword(String errConfirmPassword) {
        this.errConfirmPassword = errConfirmPassword;
    }

    public String getErrName() {
        return errName;
    }

    public void setErrName(String errName) {
        this.errName = errName;
    }

    public String getErrPhoneNumber() {
        return errPhoneNumber;
    }

    public void setErrPhoneNumber(String errPhoneNumber) {
        this.errPhoneNumber = errPhoneNumber;
    }

    public String getErrAddress() {
        return errAddress;
    }

    public void setErrAddress(String errAddress) {
        this.errAddress = errAddress;
    }
    
    
}
