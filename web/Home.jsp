<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include  file="header.jsp"%>
<!DOCTYPE html>
<html>
    <body>
        <!--begin of menu-->
        <section class="jumbotron text-center">
            <div class="container">
                <h1 class="jumbotron-heading">Siêu thị đồ công nghệ cao</h1>
                <p class="lead text-muted mb-0">Uy tín tạo nên thương hiệu với hơn 10 năm cung cấp các sản phầm từ Việt Nam</p>
            </div>
        </section>

        <!--end of menu-->
        <div class="container">
            <div class="row">
                <div class="col">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="viewproductservlet">Home</a></li>
                            <li class="breadcrumb-item"><a href="viewproductservlet">${MSG}</a></li>
                            <li class="breadcrumb-item"><a href="viewproductservlet">${ERROR}</a></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="card bg-light mb-3">
                        <div class="card-header bg-primary text-white text-uppercase"><i class="fa fa-list"></i>Categories</div>
                        <ul class="list-group category_block">                           
                            <c:forEach items="${listCategory}" var="o">
                                <input type="text" name="cateId" value="${o.id}" hidden=""/>
                                <li class="list-group-item text-white"><a href="MainController?action=SearchByCate&cateId=${o.id}">${o.name}</a></li>
                                </c:forEach>
                        </ul>
                    </div>   

                    <div class="card bg-light mb-3">
                        <div class="card-header bg-success text-white text-uppercase">Last product</div>
                        <div class="card-body">
                            <img class="img-fluid" src="${p.image}" />
                            <h5 class="card-title">${p.name}</h5>
                            <p class="card-text">${p.title}</p>
                            <p class="bloc_left_price">${p.price} $</p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-9">
                    <div class="row">

                        <c:forEach items="${listProduct}" var="p">
                            <c:if test="${p.status == true}">
                                <div class="col-12 col-md-6 col-lg-4">
                                    <div class="card">
                                        <input type="text" name="id" value="${p.id}" hidden=""/>
                                        <img class="card-img-top" src="${p.imageLink}" alt="Card image cap">
                                        <div class="card-body">
                                            <h4 class="card-title show_txt"><a href="SearchByIdServlet?Id=${p.id}" title="View Product">${p.name}</a></h4>
                                            <div class="row">
                                                <div class="col">
                                                    <p class="btn btn-danger btn-block">${p.price} $</p>
                                                </div>
                                                <div class="col">
                                                    <a href="MainController?action=AddToCart&id=${p.id}" class="btn btn-success btn-block">Add to cart</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:if>
                        </c:forEach>

                    </div>
                </div>

            </div>
        </div>

    </body>
</html>
<%@include  file="footer.jsp" %>


