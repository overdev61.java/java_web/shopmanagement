package daos;

import dtos.Category;
import dtos.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import utilities.DBUtil;

public class ProductDAO {

    private Connection con = null;
    private PreparedStatement pstm = null;
    private ResultSet rs = null;

    public void closeConnection() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (pstm != null) {
            pstm.close();
        }
        if (con != null) {
            con.close();
        }
    }

    public List<Product> getListProduct() throws SQLException {
        List<Product> list = new ArrayList<>();
        try {
            con = DBUtil.getConnection();
            if (con != null) {
                String sql = " SELECT id, name, imageLink, description, createDate, price, quantity, status, categoryID "
                        + " FROM tblProduct "
                        + " order by cast (id as int) ";
                pstm = con.prepareStatement(sql);
                rs = pstm.executeQuery();
                while (rs.next()) {
                    String id = rs.getString("id");
                    String name = rs.getString("name");
                    String imageLink = rs.getString("imageLink");
                    String description = rs.getString("description");
                    String createDate = rs.getString("createDate");
                    int price = rs.getInt("price");
                    int quantity = rs.getInt("quantity");
                    boolean status = rs.getBoolean("status");
                    String categoryID = rs.getString("categoryID");

                    CategoryDAO cateDAO = new CategoryDAO();
                    Category cate = cateDAO.getCategoryID(categoryID);

                    Product pro = new Product(id, name, imageLink, description, createDate, price, quantity, status, cate);
                    list.add(pro);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return list;
    }

    public boolean updateProduct(Product pro) throws SQLException {
        boolean check = false;
        try {
            con = DBUtil.getConnection();
            if (con != null) {
                String sql = " UPDATE tblProduct "
                        + " SET name = ?, imageLink = ?, description = ?, price = ?, quantity = ?, status = ?"
                        + " WHERE id = ? ";
                pstm = con.prepareStatement(sql);

                pstm.setString(1, pro.getName());
                pstm.setString(2, pro.getImageLink());
                pstm.setString(3, pro.getDescription());
                pstm.setInt(4, pro.getPrice());
                pstm.setInt(5, pro.getQuantity());
                pstm.setBoolean(6, pro.isStatus());
                pstm.setString(7, pro.getId());
                check = pstm.executeUpdate() > 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return check;
    }

    public Product getProductById(String id) throws SQLException {
        Product p = null;
        try {
            con = DBUtil.getConnection();
            if (con != null) {
                String sql = " SELECT id, name, imageLink, description, createDate, price, quantity, status, categoryID "
                        + " FROM tblProduct "
                        + " WHERE id = ? ";
                pstm = con.prepareStatement(sql);
                pstm.setString(1, id);
                rs = pstm.executeQuery();
                while (rs.next()) {
                    String proId = rs.getString("id");
                    String name = rs.getString("name");
                    String imageLink = rs.getString("imageLink");
                    String description = rs.getString("description");
                    String createDate = rs.getString("createDate");
                    int price = rs.getInt("price");
                    int quantity = rs.getInt("quantity");
                    boolean status = rs.getBoolean("status");
                    String categoryID = rs.getString("categoryID");

                    CategoryDAO cateDAO = new CategoryDAO();
                    Category cate = cateDAO.getCategoryID(categoryID);

                    p = new Product(proId, name, imageLink, description, createDate, price, quantity, status, cate);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return p;
    }

    public int getLastProductID() throws Exception {
        int id = 0;
        try {
            con = DBUtil.getConnection();
            String sql = "SELECT MAX(CAST(id AS INT)) AS id FROM dbo.tblProduct";

            pstm = con.prepareStatement(sql);
            rs = pstm.executeQuery();
            if (rs.next()) {
                id = rs.getInt("id");
            }
        } finally {
            closeConnection();
        }
        return id;
    }

    public boolean createProduct(Product p) throws SQLException {
        boolean check = false;
        try {
            con = DBUtil.getConnection();
            if (con != null) {
                String sql = " INSERT INTO tblProduct "
                        + " VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?) ";

                pstm = con.prepareStatement(sql);
                pstm.setString(1, p.getId());
                pstm.setString(2, p.getName());
                pstm.setString(3, p.getImageLink());
                pstm.setString(4, p.getDescription());
                pstm.setString(5, p.getCreateDate());
                pstm.setInt(6, p.getPrice());
                pstm.setInt(7, p.getQuantity());
                pstm.setBoolean(8, p.isStatus());
                pstm.setString(9, p.getCategoryID().getId());

                check = pstm.executeUpdate() > 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return check;
    }

    public List<Product> searchProductByCate(String cateID) {
        List<Product> list = new ArrayList<>();
        try {
            for (Product p : getListProduct()) {
                if (p.getCategoryID().getId().contains(cateID)) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
        }
        return list;
    }
        public List<Product> searchProductByName(String name) {
        List<Product> list = new ArrayList<>();
        try {
            for (Product p : getListProduct()) {
                if (p.getName().contains(name)) {
                    list.add(p);
                }
            }
        } catch (Exception e) {
        }
        return list;
    }
}
