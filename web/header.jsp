<%-- 
    Document   : header
    Created on : Jun 26, 2022, 6:36:56 PM
    Author     : asus
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!------ Include the above in your HEAD tag ---------->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>

        <nav class="navbar navbar-expand-md navbar-dark bg-dark">
            <div class="container">
                <a class="navbar-brand" href="viewproductservlet">S5-TECHNOLOGY</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse justify-content-end" id="navbarsExampleDefault">

                    <form action="MainController" method="POST" class="form-inline my-2 my-lg-0">
                        <div class="input-group input-group-sm">
                            <input name="name" type="text" value="" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" placeholder="Search...">
                            <div class="input-group-append">
                                <button type="submit" name="action" value="SearchByName" class="btn btn-secondary btn-number">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>

                        <a class="btn btn-success btn-sm ml-3" href="Cart.jsp">
                            <i class="fa fa-shopping-cart"></i> Cart
                            <span class="badge badge-light"></span>
                        </a>


                    </form>
                    <c:choose>
                        <c:when test="${sessionScope.USER_DATA !=null}">
                            <a class="btn btn-success btn-sm ml-3" href="MainController?action=Logout">
                                Logout
                            </a>
                        </c:when>
                        <c:when test="${sessionScope.USER_DATA ==null}">
                            <a class="btn btn-success btn-sm ml-3" href="MainController?action=Login">
                                Login
                            </a>
                        </c:when>
                    </c:choose>
                    <c:if test="${sessionScope.USER_DATA.isAdmin == true}">
                        <a class="btn btn-success btn-sm ml-3" href="ManageProductServlet">
                            Admin Page
                        </a>
                    </c:if>
                </div>
            </div>
        </nav>

    </body>
</html>
