
package daos;

import utilities.DBUtil;
import dtos.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDao {

    Connection connection = null;
    PreparedStatement preparedStatement = null;
    ResultSet resultSet = null;

    private void closeConnection() throws SQLException {
        if (resultSet != null) {
            resultSet.close();
        }
        if (preparedStatement != null) {
            preparedStatement.close();
        }
        if (connection != null) {
            connection.close();
        }
    }

    public User checkLogin(String email, String password) throws Exception {
        User user = null;
        try {
            connection = DBUtil.getConnection();
            String sql = " SELECT id, name, password, email, phoneNumber, isAdmin, address "
                    + " FROM tblUser "
                    + " WHERE email LIKE ? AND password LIKE ? ";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);
            resultSet = preparedStatement.executeQuery();
            user = new User();
            if (resultSet.next()) {
                user.setId(resultSet.getString("id"));
                user.setName(resultSet.getNString("name"));
                user.setPhoneNumber(resultSet.getString("phoneNumber"));
                user.setAddress(resultSet.getNString("address"));
                user.setIsAdmin(resultSet.getBoolean("isAdmin"));
            }
        } finally {
            closeConnection();
        }
        return user;
    }

    public boolean createUser(User user) throws SQLException {
        boolean check = false;
        try {
            connection = DBUtil.getConnection();
            if (connection != null) {
                String sql = " INSERT INTO tblUser(id, name, password, email, phoneNumber, isAdmin, address) "
                        + " VALUES(?,?,?,?,?,?,?) ";
                preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setString(1, user.getId());
                preparedStatement.setString(2, user.getName());
                preparedStatement.setString(3, user.getPassword());
                preparedStatement.setString(4, user.getEmail());
                preparedStatement.setString(5, user.getPhoneNumber());
                preparedStatement.setBoolean(6, user.isIsAdmin());
                preparedStatement.setString(7, user.getAddress());
                check = preparedStatement.executeUpdate() > 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return check;
    }

    public boolean checkDuplicateId(String id) throws SQLException {
        User user = null;
        boolean check = false;
        try {
            connection = DBUtil.getConnection();
            if (connection != null) {
                String sql = " SELECT id "
                        + " FROM tlbUser "
                        + " WHERE id=? ";
                preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setString(1, id);
                resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    check = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return check;
    }
        public boolean checkDuplicateEmail(String email) throws SQLException {
        boolean check = false;
        try {
            connection = DBUtil.getConnection();
            if (connection != null) {
                String sql = " SELECT email "
                        + " FROM tblUser "
                        + " WHERE email=? ";
                preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setString(1, email);
                resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    check = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return check;
    }
}
