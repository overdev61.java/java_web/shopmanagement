/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import dtos.Category;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import utilities.DBUtil;

/**
 *
 * @author asus
 */
public class CategoryDAO {

    private Connection con = null;
    private PreparedStatement pstm = null;
    private ResultSet rs = null;

    public void closeConnection() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (pstm != null) {
            pstm.close();
        }
        if (con != null) {
            con.close();
        }
    }

    public Category getCategoryID(String id) throws SQLException {
        Category cate = null;
        try {
            con = DBUtil.getConnection();
            if (con != null) {
                String sql = "SELECT id, name "
                        + " FROM tblCategory "
                        + " WHERE id=? ";
                pstm = con.prepareStatement(sql);
                pstm.setString(1, id);
                rs = pstm.executeQuery();
                while (rs.next()) {
                    String categoryID = rs.getString("id");
                    String name = rs.getString("name");
                    cate = new Category(categoryID, name);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return cate;
    }

    public List<Category> getListCategory() throws SQLException {
        List<Category> list = new ArrayList<>();
        try {
            con = DBUtil.getConnection();
            if (con != null) {
                String sql = " SELECT id, name "
                        + " FROM tblCategory ";
                pstm = con.prepareStatement(sql);
                rs=pstm.executeQuery();
                while(rs.next()){
                    String id = rs.getString("id");
                    String name=rs.getString("name");
                    
                    Category cate = new Category(id, name);
                    list.add(cate);
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            closeConnection();
        }
        return list;
    }
    
    
}
