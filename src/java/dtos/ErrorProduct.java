/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dtos;

/**
 *
 * @author hoaia
 */
public class ErrorProduct {

    private String errName;
    private String errImageLink;
    private String errDescription;
    private String errPrice;
    private String errQuantity;

    public ErrorProduct() {
    }

    public ErrorProduct(String errName, String errImageLink, String errDescription, String errPrice, String errQuantity) {
        this.errName = errName;
        this.errImageLink = errImageLink;
        this.errDescription = errDescription;
        this.errPrice = errPrice;
        this.errQuantity = errQuantity;
    }

    public String getErrName() {
        return errName;
    }

    public void setErrName(String errName) {
        this.errName = errName;
    }

    public String getErrImageLink() {
        return errImageLink;
    }

    public void setErrImageLink(String errImageLink) {
        this.errImageLink = errImageLink;
    }

    public String getErrDescription() {
        return errDescription;
    }

    public void setErrDescription(String errDescription) {
        this.errDescription = errDescription;
    }

    public String getErrPrice() {
        return errPrice;
    }

    public void setErrPrice(String errPrice) {
        this.errPrice = errPrice;
    }

    public String getErrQuantity() {
        return errQuantity;
    }

    public void setErrQuantity(String errQuantity) {
        this.errQuantity = errQuantity;
    }

       
}
