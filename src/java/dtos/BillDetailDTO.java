package dtos;

public class BillDetailDTO {

    private String id;
    private String billID;
    private Product product;
    private int quantity;
    private int price;

    public BillDetailDTO() {
    }

    public BillDetailDTO(String id, String billID, Product product, int quantity, int price) {
        this.id = id;
        this.billID = billID;
        this.product = product;
        this.quantity = quantity;
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBillID() {
        return billID;
    }

    public void setBillID(String billID) {
        this.billID = billID;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    
}
