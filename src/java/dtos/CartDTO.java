package dtos;

import java.util.HashMap;
import java.util.Map;

public class CartDTO {

    private Map<String, Product> cart;

    public CartDTO() {
    }

    public CartDTO(Map<String, Product> cart) {
        this.cart = cart;
    }

    public Map<String, Product> getCart() {
        return cart;
    }

    public void setCart(Map<String, Product> cart) {
        this.cart = cart;
    }

    public boolean add(Product pro) {
        boolean check = false;
        try {
            if (cart == null) {
                cart = new HashMap<>();
            }
            if (cart.containsKey(pro.getId())) {
                int quantity = cart.get(pro.getId()).getQuantity();
                pro.setQuantity(quantity + pro.getQuantity());
            }
            cart.put(pro.getId(), pro);
            check = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return check;
    }

    public void delete(String id) {
        try {
            if (cart != null) {
                if (cart.containsKey(id)) {
                    cart.remove(id);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();        }
    }
}
